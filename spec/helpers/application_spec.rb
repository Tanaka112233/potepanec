require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    context "引数なしの場合" do
      it "「BIGBAG Store」のみを表示" do
        expect(full_title).to eq "BIGBAG Store"
      end
    end

    context "引数ありの場合" do
      it "「引数 - BIGBAG Store」を表示" do
        expect(full_title(page_title: "Test")).to eq "Test - BIGBAG Store"
      end
    end

    context "引数がnilの場合" do
      it "「BIGBAG Store」のみを表示" do
        expect(full_title(page_title: nil)).to eq "BIGBAG Store"
      end
    end
  end
end
