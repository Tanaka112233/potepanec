require 'rails_helper'

RSpec.describe CategoriesHelper, type: :helper do
  describe "display_taxon" do
    let(:taxon1) { create :taxon }
    let(:taxon2) { create :taxon }
    let!(:product1) { create(:product, taxons: [taxon1]) }

    context "taxonにproductが設定されている場合" do
      it "trueを返す" do
        expect(display_taxon?(taxon1)).to be_truthy
      end
    end

    context "taxonにproductが設定されていない場合" do
      it "falseを返す" do
        expect(display_taxon?(taxon2)).to be_falsey
      end
    end
  end
end
