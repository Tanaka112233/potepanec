require "rails_helper"

RSpec.describe "ページ遷移", type: :system, selenium: true do
  let!(:taxonomies) { [create(:taxonomy, name: 'Categories'), create(:taxonomy, name: 'Brand')] }
  let!(:taxon1) { create :taxon, name: 'category_child', taxonomy: taxonomies[0], parent_id: taxonomies[0].root.id }
  let!(:taxon2) { create :taxon, name: 'brand_child', taxonomy: taxonomies[1], parent_id: taxonomies[1].root.id }
  let!(:product1) { create(:product, taxons: [taxon1, taxon2]) }
  let!(:product2) { create(:product) }

  it "カテゴリーをクリックした時、適切なカテゴリーのページに遷移すること", js: true do
    visit potepan_category_path(taxon1.id)
    click_on 'Brand'
    click_on 'brand_child'
    expect(current_path).to eq potepan_category_path(taxon2.id)
    click_on 'Categories'
    click_on 'category_child'
    expect(current_path).to eq potepan_category_path(taxon1.id)
  end

  it "製品をクリックした時に、正しい製品詳細ページに遷移後、適切なカテゴリーのページに戻れること" do
    visit potepan_category_path(taxon2.id)
    click_on product1.name
    expect(current_path).to eq potepan_product_path(product1.id)
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon2.id)
  end

  it "直接taxonが紐づけられている製品詳細ページに訪れた場合、taxonによって紐づけられたページへ戻れること" do
    visit potepan_product_path(product1.id)
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon1.id)
  end

  it "直接taxonが紐づけられていない製品詳細ページに訪れた場合、Categriseのページに戻れること" do
    visit potepan_product_path(product2.id)
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(1)
  end
end
