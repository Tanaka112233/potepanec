require 'rails_helper'

RSpec.describe "Products", type: :system, selenium: true do
  let(:product1) { create(:product, variants: [variant1], taxons: [taxon1]) }
  let(:taxon1) { create(:taxon, products: [*related_products1]) }
  let(:related_products1) { create_list(:product, 4) }
  let(:variant1) { create :variant, images: [image1] }
  let(:image1) { create :image }

  let(:product2) { create(:product, taxons: [taxon2]) }
  let(:taxon2) { create(:taxon) }

  describe "表示のテスト" do
    it "商品詳細が正しく表示されること" do
      aggregate_failures do
        visit potepan_product_path(product1.id)
        within ".media" do
          expect(page).to have_content(product1.name)
          expect(page).to have_content(product1.price.to_s)
          product1.master_images.each do |master_image|
            expect(page).to have_content(master_image.attachment_file_name)
          end
        end
      end
    end

    context "関連商品がある場合" do
      it "関連商品が全て表示されること" do
        visit potepan_product_path(product1.id)
        within(".productsContent") do
          product1.taxons.first.products do |related_product|
            expect(page).to have_content(related_product.name)
          end
        end
      end
    end

    context "関連商品がない場合" do
      it "関連表示欄に何も表示しないこと" do
        visit potepan_product_path(product2.id)
        within(".productsContent") do
          expect(page).not_to have_selector(".productBox")
        end
      end
    end

    it "Homeへのリンクが3つあること" do
      visit potepan_product_path(product1.id)
      expect(page).to have_link(href: potepan_path, count: 3)
    end
  end

  describe "ページ遷移のテスト" do
    it "関連商品をクリックした時、適切な商品のページに遷移する。その後「一覧ページに戻る」をクリックした時、商品一覧ページに戻れること" do
      visit potepan_product_path(product1.id)
      within(".productsContent") do
        click_on related_products1[0].name
        expect(current_path).to eq potepan_product_path(related_products1[0].id)
      end
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon1.id)
    end
  end
end
