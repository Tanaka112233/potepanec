require 'rails_helper'

RSpec.describe "potepan/categories", type: :request do
  let(:taxon) { create(:taxon) }

  describe "showアクションのテスト" do
    before do
      get potepan_category_path(taxon.id)
    end

    it "正しいリクエストを送れていること" do
      expect(request[:controller]).to eq "potepan/categories"
      expect(request[:action]).to eq "show"
    end

    it "正常なレスポンスを返すこと" do
      expect(response).to have_http_status(200)
    end

    it "showテンプレートが返ってきていること" do
      expect(response).to render_template(:show)
    end
  end
end
