require 'rails_helper'

RSpec.describe "potepan/products", type: :request do
  let(:product) { create(:product) }

  describe "showアクションのテスト" do
    before do
      get potepan_product_path(product.id)
    end

    it "正しいリクエストを送れていること" do
      expect(request[:controller]).to eq "potepan/products"
      expect(request[:action]).to eq "show"
    end

    it "正常なレスポンスを返すこと" do
      expect(response).to have_http_status(200)
    end

    it "showテンプレートが返ってきていること" do
      expect(response).to render_template(:show)
    end
  end
end
