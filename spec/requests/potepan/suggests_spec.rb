require 'rails_helper'

RSpec.describe "potepan/suggests", type: :request do
  describe "indexアクションのテスト" do
    let(:stub_connection) do
      Faraday.new do |conn|
        conn.adapter :test, Faraday::Adapter::Test::Stubs.new do |stub|
          stub.get(ENV['POTEPAN_WEBAPI_PATH']) do
            [200, {}, %w(test stub)]
          end
        end
      end
    end

    let(:response) do
      suggests_controller = Potepan::SuggestsController.new
      allow(suggests_controller).to receive(:index).and_return(stub_connection.get(ENV['POTEPAN_WEBAPI_PATH']))
      suggests_controller.index
    end

    it "正しいリクエストを送れていること" do
      get potepan_suggests_path, params: { keyword: "r" }
      expect(request[:controller]).to eq "potepan/suggests"
      expect(request[:action]).to eq "index"
    end

    it "正常なレスポンスを返すこと" do
      expect(response.status).to eq 200
    end
  end
end
