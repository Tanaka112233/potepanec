require 'rails_helper'

RSpec.describe "potepan/api/suggests", type: :request do
  describe "indexアクションのテスト" do
    let(:json) { JSON.parse(response.body) }
    let(:word_suggested_in_default) { ["ruby", "ruby for women", "ruby for men"] }

    it "正しいリクエストを送れていること" do
      suggests_request
      expect(request[:controller]).to eq "potepan/api/suggests"
      expect(request[:action]).to eq "suggests_response"
    end

    context "トークンもパラメータも正しい場合" do
      it "正常なレスポンスを返すこと" do
        suggests_request
        expect(response.status).to eq 200
      end

      it "keywordに関連するサジェストが5つ返ってくること" do
        suggests_request
        expect(json).to contain_exactly("ruby", "ruby for women", "ruby for men", "rails", "rails for women")
      end
    end

    context "トークンは間違っているがパラメータは正しい場合" do
      it "認証エラーを返すこと" do
        suggests_request(authorization: "Bearer test")
        expect(response.status).to eq 401
      end
    end

    context "トークンが存在していない場合" do
      it "認証エラーを返すこと" do
        suggests_request(authorization: "")
        expect(response.status).to eq 401
      end
    end

    context "トークンは正しいが検索ワードがデータベースに存在しないものの場合" do
      it "正常なレスポンスを返すこと" do
        suggests_request(keyword: "test")
        expect(response.status).to eq 200
      end

      it "空のデータを返すこと" do
        suggests_request(keyword: "test")
        expect(json).to be_empty
      end
    end

    context "トークンは正しいが検索ワードが存在しない場合" do
      it "サーバエラーを返すこと" do
        request_with_no_keywords
        expect(response.status).to eq 500
      end
    end

    context "トークンは正しいが最大取得件数が0の場合" do
      it "正常なレスポンスを返すこと" do
        suggests_request(max_num: 0)
        expect(response.status).to eq 200
      end

      it "keywordに関連するサジェストが3つ返ってくること" do
        suggests_request(max_num: 0)
        expect(json).to contain_exactly(*word_suggested_in_default)
      end
    end

    context "トークンは正しいが最大取得件数が存在しない場合" do
      it "正常なレスポンスを返すこと" do
        requests_with_no_acquisition_count
        expect(response.status).to eq 200
      end

      it "keywordに関連するサジェストが3つ返ってくること" do
        requests_with_no_acquisition_count
        expect(json).to contain_exactly(*word_suggested_in_default)
      end
    end
  end
end
