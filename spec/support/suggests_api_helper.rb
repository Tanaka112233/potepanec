module SuggestsApiHelpers
  def suggests_request(authorization: "Bearer #{ENV["TASK5_2_WEBAPI_KEY"]}", keyword: "r", max_num: 5)
    get potepan_api_suggests_path, headers: { Authorization: authorization }, params: { keyword: keyword, max_num: max_num }
  end

  def request_with_no_keywords
    get potepan_api_suggests_path, headers: { Authorization: "Bearer #{ENV["TASK5_2_WEBAPI_KEY"]}" }, params: { max_num: 5 }
  end

  def requests_with_no_acquisition_count
    get potepan_api_suggests_path, headers: { Authorization: "Bearer #{ENV["TASK5_2_WEBAPI_KEY"]}" }, params: { keyword: "r" }
  end
end
