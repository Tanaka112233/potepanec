require 'rails_helper'

RSpec.describe Spree::Product, type: :decorator do
  describe "related_products" do
    let(:product1) { create(:product, taxons: [taxon1]) }
    let(:taxon1) { create(:taxon, products: [*related_products1]) }
    let(:related_products1) { create_list(:product, 10) }

    let(:product2) { create(:product, taxons: [taxon2]) }
    let(:taxon2) { create(:taxon, products: [*related_products2]) }
    let(:related_products2) { create_list(:product, 4) }

    let(:product3) { create(:product, taxons: [taxon3]) }
    let(:taxon3) { create(:taxon) }

    let(:product4) { create(:product, taxons: [taxon4_1, taxon4_2]) }
    let(:taxon4_1) { create(:taxon, products: [related_product4_1, related_product4_2, related_product4_3, related_product4_4, high_related_product]) }
    let(:taxon4_2) { create(:taxon) }
    let(:related_product4_1) { create(:product) }
    let(:related_product4_2) { create(:product) }
    let(:related_product4_3) { create(:product) }
    let(:related_product4_4) { create(:product) }
    let(:high_related_product) { create(:product, taxons: [taxon4_2]) }

    context "関連商品が4つ以上ある場合" do
      it "4つまでしか格納されないこと" do
        expect(product1.related_products).to include(related_products1[0])
        expect(product1.related_products).to include(related_products1[3])
        expect(product1.related_products).not_to include(related_products1[4])
        expect(product1.related_products).not_to include(related_products1[9])
      end
    end

    context "関連商品が4つ以下の場合" do
      it "全て格納すること" do
        product2.taxons.first.products do |related_product|
          expect(product2.related_products).to include(related_product)
        end
      end
    end

    context "関連商品がない場合" do
      it "何も格納されないこと" do
        expect(product3.related_products).to be_empty
      end
    end

    it "関連元の商品が格納されないこと" do
      expect(product3.related_products).not_to include(product3)
    end

    it "関連度の高いproductが優先的に格納されること" do
      expect(product4.related_products).to match([high_related_product, related_product4_1, related_product4_2, related_product4_3])
    end
  end
end
