require 'rails_helper'

RSpec.describe "layouts/application.html.slim", type: :view do
  before do
    render
  end

  describe "表示のテスト" do
    it "サイトロゴが表示されていること" do
      assert_select "img[alt=BIGBAG]"
    end
    it "titleが正しく表示されること" do
      assert_select "title", "BIGBAG Store"
    end
    it "コピーライトマークが表示されていること" do
      expect(rendered).to match("&copy;")
    end
  end

  describe "リンクのテスト" do
    it "Homeへのリンクが2つあること" do
      assert_select "a[href=?]", potepan_path, count: 2
    end
  end
end
