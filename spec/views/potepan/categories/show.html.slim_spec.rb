require 'rails_helper'

RSpec.describe "potepan/categories/show.html.slim", type: :view do
  before do
    assign(:taxonomies, taxonomies)
    assign(:taxon, taxon1)
    assign(:products, taxon1.all_products)
    render
  end

  let!(:taxonomies) { [create(:taxonomy, name: 'Categories'), create(:taxonomy, name: 'Brand')] }
  let!(:taxon1) { create :taxon, name: 'category_child', taxonomy: taxonomies[0], parent_id: taxonomies[0].root.id, products: [product1, product2] }
  let!(:taxon2) { create :taxon, name: 'brand_child', taxonomy: taxonomies[1], parent_id: taxonomies[1].root.id }
  let!(:product1) { create :product, master: variant1 }
  let!(:product2) { create :product, master: variant2 }
  let!(:variant1) { create :variant, images: [image1] }
  let!(:variant2) { create :variant, images: [image2] }
  let!(:image1) { create :image }
  let!(:image2) { create :image }

  describe "表示のテスト" do
    subject { rendered }

    it "Taxonomiesが全て表示されていること" do
      taxonomies.each do |taxonomy|
        is_expected.to match(taxonomy.name)
      end
    end

    it "Taxonsが全て表示されていること" do
      taxonomies.each do |taxonomy|
        taxonomy.taxons.each do |taxon|
          is_expected.to match(taxon.name)
        end
      end
    end

    it "商品が全て表示されていること" do
      expect(taxon1.all_products).not_to be_empty
      taxon1.all_products.each do |product|
        is_expected.to match(product.name)
        is_expected.to match(product.display_image.attachment_file_name)
        is_expected.to match(product.price.to_s)
      end
    end

    it "パートナーズロゴが表示されていること" do
      (1..5).each do |num|
        is_expected.to match("partner-0#{num}")
      end
    end
  end

  describe "リンクのテスト" do
    it "Homeへのリンクが1つあること" do
      assert_select "a[href=?]", potepan_path, count: 1
    end
  end
end
