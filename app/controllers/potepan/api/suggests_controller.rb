class Potepan::Api::SuggestsController < ApplicationController
  include ActionController::HttpAuthentication::Token::ControllerMethods
  WORD_COUNT_SUGGESTED_IN_DEFAULT = 3
  before_action :authenticate

  def suggests_response
    raise unless params[:keyword]
    render json: suggest_words
  end

  private

  def authenticate
    authenticate_or_request_with_http_token do |token|
      ActiveSupport::SecurityUtils.secure_compare(token, ENV['TASK5_2_WEBAPI_KEY'])
    end
  end

  def suggest_words
    acquisition_count = params[:max_num] =~ /\A[1-9][0-9]*\z/ ? params[:max_num] : WORD_COUNT_SUGGESTED_IN_DEFAULT
    Potepan::Suggest.where("keyword LIKE(?)", "#{params[:keyword]}%").limit(acquisition_count).pluck("keyword")
  end
end
