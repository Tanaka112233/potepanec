class Potepan::SuggestsController < ApplicationController
  def index
    connection = Faraday.new(ENV['POTEPAN_WEBAPI_HOST'])
    response =
      connection.get ENV['POTEPAN_WEBAPI_PATH'] do |request|
        request.headers['Authorization'] = "Bearer #{ENV['POTEPAN_WEBAPI_KEY']}"
        request.params[:keyword] = params[:keyword]
        request.params[:max_num] = 5
      end
    response.status == Rack::Utils::SYMBOL_TO_STATUS_CODE[:ok] ? (render json: response.body) : (head response.status)
  end
end
