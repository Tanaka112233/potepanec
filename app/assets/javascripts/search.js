$(document).ready(function () {
  $('input[name="searchWord"]').autocomplete({
    source: function (request, response) {
      $.get("/potepan/suggests", {
        keyword: request.term,
      })
        .done(function (data) {
          response(data);
        })
        .fail({})
    },
    autoFocus: true,
    delay: 300,
    minLength: 1
  })

  $('input[name="searchWord"]')
    .hover(function (e) {
      e.stopPropagation();
    })
    .change(function () {
      $('#searchBox').dropdown('toggle');
    });
});