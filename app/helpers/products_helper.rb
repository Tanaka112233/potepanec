module ProductsHelper
  def list_page(product)
    if request.referrer && Rails.application.routes.recognize_path(request.referrer)[:controller] == "potepan/categories"
      request.referrer
    else
      potepan_category_path(product.taxons.first&.id || 1)
    end
  end
end
