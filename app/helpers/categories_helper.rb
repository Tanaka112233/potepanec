module CategoriesHelper
  def display_taxon?(taxon)
    taxon.products.count.nonzero?
  end
end
