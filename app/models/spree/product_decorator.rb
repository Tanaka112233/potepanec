module Spree::ProductDecorator
  Spree::Product.include self
  RELATED_PRODUCT_MAX_DISPLAY_NUMBER = 4

  # @productと関連性が高い（taxonsでの繋がりが多い）商品を先頭にソートした関連商品を返す
  def related_products
    Spree::Product.includes(master: [:default_price, :images]).
      in_taxons(taxons).
      where.not(id: id).
      group_by { |product| product }.
      sort_by { |key, value| -value.size }.
      take(RELATED_PRODUCT_MAX_DISPLAY_NUMBER).
      map(&:first)
  end
end
